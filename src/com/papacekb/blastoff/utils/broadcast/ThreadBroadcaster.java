package com.papacekb.blastoff.utils.broadcast;

import android.os.Handler;

public class ThreadBroadcaster extends Broadcaster {

	private Handler handler;

	public ThreadBroadcaster(Handler handler) {
		this.handler = handler;
	}

	@Override
	public void fireEvent(final BroadcastEvent e) {
		handler.post(new Runnable() {
			@Override
			public void run() {
				ThreadBroadcaster.super.fireEvent(e);
			}
		});
	}
}