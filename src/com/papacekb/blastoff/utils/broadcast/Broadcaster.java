package com.papacekb.blastoff.utils.broadcast;

import java.util.HashSet;
import java.util.Set;

public class Broadcaster {
	
	private Set<BroadcastListener> listeners;
	
	public Broadcaster() {
		this.listeners = new HashSet<BroadcastListener>();
	}
	
	public void addBroadcastListener(BroadcastListener listener) {
		this.listeners.add(listener);
	}
	
	public void removeListener(BroadcastListener listener) {
		this.listeners.remove(listener);
	}
	
	public void fireEvent(BroadcastEvent e) {
		for(BroadcastListener listener : this.listeners) {
			listener.handleBroadcastEvent(e);
		}
	}

}
