package com.papacekb.blastoff.utils.broadcast;

import java.util.HashMap;
import java.util.HashSet;

public class BroadcastEvent {
	
	private Object source;
	
	private boolean myBoolean;
	private int myInt;
	private float myFloat;
	private double myDouble;
	private long myLong;
	private String myString;
	private Enum<?> myEnum;
	private Object myObject;
	
	//these are only initialised as required
	private HashMap<String, Object> params;
	private HashSet<Object> signatures;
	
	public BroadcastEvent(Object source) {
		this.source = source;
	}
	
	//copy constructor - shallow copy only
	public BroadcastEvent(BroadcastEvent e) {
		this.source = e.source;
		this.myBoolean = e.myBoolean;
		this.myInt = e.myInt;
		this.myFloat = e.myFloat;
		this.myDouble = e.myDouble;
		this.myLong = e.myLong;
		this.myString = e.myString;
		this.myEnum = e.myEnum;
		this.myObject = e.myObject;
		if (e.params != null)
			this.params = new HashMap<String, Object>(e.params);
		if (e.signatures != null)
			this.signatures = new HashSet<Object>(e.signatures);
	}
	
	//returns this so calls can be chained
	//eg new BroadcastEvent(...).set(k1, v1).set(k2, v2).set(...
	public BroadcastEvent set(String key, Object val) {
		if (params == null)  //only initialise map if needed
			params = new HashMap<String, Object>();
		params.put(key, val);
		
		return this; 
	}
	
	public BroadcastEvent setSource(Object source) {
		this.source = source;
		return this;
	}
	
	public BroadcastEvent setBoolean(boolean b) {
		this.myBoolean = b;
		return this;
	}
	
	public BroadcastEvent setInt(int i) {
		this.myInt = i;
		return this;
	}
	
	public BroadcastEvent setFloat(float f) {
		this.myFloat = f;
		return this;
	}
	
	public BroadcastEvent setDouble(double d) {
		this.myDouble = d;
		return this;
	}
	
	public BroadcastEvent setLong(long l) {
		this.myLong = l;
		return this;
	}
	
	public BroadcastEvent setString(String s) {
		this.myString = s;
		return this;
	}
	
	public BroadcastEvent setEnum(Enum<?> e) {
		this.myEnum = e;
		return this;
	}
	
	public BroadcastEvent setObject(Object o) {
		this.myObject = o;
		return this;
	}
	
	public Object getSource() {
		return source;
	}
	
	public boolean getBoolean() {
		return myBoolean;
	}
	
	public int getInt() {
		return myInt;
	}
	
	public float getFloat() {
		return myFloat;
	}
	
	public double getDouble() {
		return myDouble;
	}
	
	public long getLong() {
		return myLong;
	}
	
	public String getString() {
		return myString;
	}
	
	public Enum<?> getEnum() {
		return myEnum;
	}
	
	public Object getObject() {
		return myObject;
	}
	
	public Object get(String key) {
		return params == null ? null : params.get(key);
	}
	
	public void sign(Object o) {
		if (signatures == null)
			signatures = new HashSet<Object>();
		signatures.add(o);
	}
	
	public boolean isSigned(Object o) {
		return signatures == null ? false : signatures.contains(o);
	}
}
