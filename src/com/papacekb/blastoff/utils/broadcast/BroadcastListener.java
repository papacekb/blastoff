package com.papacekb.blastoff.utils.broadcast;

import java.util.EventListener;

public interface BroadcastListener extends EventListener {

	public abstract void handleBroadcastEvent(BroadcastEvent e);

}
