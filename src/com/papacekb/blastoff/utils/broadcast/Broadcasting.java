package com.papacekb.blastoff.utils.broadcast;


public interface Broadcasting {
	
	public abstract Broadcaster getBroadcaster();

}
