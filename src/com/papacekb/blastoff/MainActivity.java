package com.papacekb.blastoff;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;

import com.papacekb.blastoff.conundrum.ConundrumActivity;
import com.papacekb.blastoff.letters.LettersActivity;
import com.papacekb.blastoff.letters.LettersSolverActivity;
import com.papacekb.blastoff.numbers.NumbersActivity;
import com.papacekb.blastoff.numbers.NumbersSolverActivity;

public class MainActivity extends Activity { 

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
//		startLettersGame(null);
//		startConundrumGame(null); 
//		startNumbersGame(null);
//		startLettersSolver(null);
//		startNumbersSolver(null);
	}
	
	public void startNumbersSolver(View view) {
		Intent intent = new Intent(this, NumbersSolverActivity.class);
		startActivity(intent);
	}
	
	public void startLettersSolver(View view) {
		Intent intent = new Intent(this, LettersSolverActivity.class);
		startActivity(intent);
	}
	
	public void startNumbersGame(View view) {
		Intent intent = new Intent(this, NumbersActivity.class);
		startActivity(intent);
	}
	
	public void startLettersGame(View view) {
		Intent intent = new Intent(this, LettersActivity.class);
		startActivity(intent);
	}
	
	public void startConundrumGame(View view) {
		Intent intent = new Intent(this, ConundrumActivity.class);
		startActivity(intent);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
