package com.papacekb.blastoff.conundrum;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.papacekb.blastoff.Constants;
import com.papacekb.blastoff.Constants.ThreadEventType;
import com.papacekb.blastoff.CountdownThread;
import com.papacekb.blastoff.LexiconDb;
import com.papacekb.blastoff.R;
import com.papacekb.blastoff.utils.broadcast.BroadcastEvent;
import com.papacekb.blastoff.utils.broadcast.BroadcastListener;

public class ConundrumActivity extends Activity implements BroadcastListener, View.OnClickListener {

	private String letters;
	private String solution;

	private boolean counting;
	private int time;

	private CountdownThread countdownThread;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_conundrum);

		if (savedInstanceState != null) {
			letters = savedInstanceState.getString("letters");
			solution = savedInstanceState.getString("solution");
			counting = savedInstanceState.getBoolean("counting");
			time = savedInstanceState.getInt("time");

			if (counting) {
				startCountdownThread(time);
				setEndListenerActive(true);
				setStatusVisible(true);
				setButtonsEnabled(false);
			} else {
				updateSolutions();
			}
			updateTiles();
			updateStatus();
		} else {
			letters = "";
			solution = "";
			counting = false;
			time = Constants.TIME;
		}
	}
	
	@Override
	public void onStop() {
		super.onStop();
		endCountdownThread();
	}

	@Override
	public void onSaveInstanceState(Bundle bundle) {
		super.onSaveInstanceState(bundle);
		bundle.putString("letters", letters);
		bundle.putString("solution", solution);
		bundle.putBoolean("counting", counting);
		bundle.putInt("time", time);
	}
	
	public void begin(View view) {
		reset();
		solution = getSolution();
		letters = getConundrum(solution);
		setEndListenerActive(true);
		updateTiles();
		setButtonsEnabled(false);
		setStatusVisible(true);
		startCountdownThread(Constants.TIME);
	}

//	public void end() {
//		endCountdownThread();
//	}
	
	@Override
	public void onClick(View v) {
		endCountdownThread();
	}

	private void setEndListenerActive(boolean b) {
		View wrapper = findViewById(R.id.wrapper);
		if (b)
			wrapper.setOnClickListener(this);
		else
			wrapper.setOnClickListener(null);
	}
	
	private void setButtonsEnabled(boolean b) {
		Button beginButton = (Button)findViewById(R.id.beginButton);
		beginButton.setEnabled(b);
	}
	
	private void setStatusVisible(boolean b) {
		View status = findViewById(R.id.status);
		status.setVisibility(b ? View.VISIBLE : View.GONE);
	}

	private void reset() {
		letters = "";
		solution = "";
		updateTiles();
		updateSolutions();
	}

	private String getSolution() {
		LexiconDb lexiconDb = new LexiconDb(this);
		SQLiteDatabase db = lexiconDb.getReadableDatabase();
		Cursor cursor;
		String sql = "SELECT word FROM lex9 WHERE conundrum = 1 ORDER BY RANDOM() LIMIT 1";
		cursor = db.rawQuery(sql, null);
		cursor.moveToFirst();
		String solution = cursor.getString(0);

		return solution;
	}

	private String getConundrum(String solution) {
		List<Character> list = new ArrayList<Character>(solution.length());
		for(char c : solution.toCharArray())
			list.add(c);
		Collections.shuffle(list);
		StringBuilder sb = new StringBuilder();
		for(char c : list)
			sb.append(Character.toUpperCase(c));
		return sb.toString();
	}

	private void updateSolutions() {
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.list_words, new String [] {solution});
		ListView list = (ListView)findViewById(R.id.solutions);
		list.setAdapter(adapter);
	}

	private void updateTiles() {
		ViewGroup tileLayout = (ViewGroup)findViewById(R.id.tileLayout);
		for(int i = 0; i < Constants.NUM_LETTERS; i++) {
			TextView tile = (TextView)tileLayout.getChildAt(i);
			String s = i < letters.length() ? "" + Character.toUpperCase(letters.charAt(i)) : "";
			tile.setText(s);
		}
	}

	private void updateStatus() {
		TextView status = (TextView)findViewById(R.id.status);
		if (time <= 0)
			status.setText("");
		else
			status.setText("" + time);
	}

	private void startCountdownThread(int time) {
		countdownThread = new CountdownThread(new Handler(), time);
		countdownThread.getBroadcaster().addBroadcastListener(this);
		counting = true;
		countdownThread.start();
	}

	private void endCountdownThread() {
		if (counting) {
			countdownThread.abort = true;
			countdownThread.interrupt();
			try {
				countdownThread.join();
			} catch(InterruptedException e) {}
		}
	}

	private void handleCountdownEvent(BroadcastEvent e) {
		if (e.getEnum() == ThreadEventType.STATUS_EVENT) {
			time = e.getInt();
			updateStatus();
		} else if (e.getEnum() == ThreadEventType.COMPLETION_EVENT) {
			time = 0;
			updateStatus();
			updateSolutions();
			setButtonsEnabled(true);
			setStatusVisible(false);
			setEndListenerActive(false);
			counting = false;
		}
	}

	@Override
	public void handleBroadcastEvent(BroadcastEvent e) {
		if (e.getSource() == countdownThread) {
			handleCountdownEvent(e);
		}
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.conundrum, menu);
		return true;
	}

}
