package com.papacekb.blastoff.numbers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import android.os.Handler;

import com.papacekb.blastoff.Constants.ThreadEventType;
import com.papacekb.blastoff.utils.broadcast.BroadcastEvent;
import com.papacekb.blastoff.utils.broadcast.Broadcaster;
import com.papacekb.blastoff.utils.broadcast.Broadcasting;
import com.papacekb.blastoff.utils.broadcast.ThreadBroadcaster;

public class NumbersSolverThread extends Thread implements Broadcasting {

	//unicode symbols for multiplication and division
	public static final char MULT_CHAR = '\u00D7';
	public static final char DIV_CHAR = '\u00F7';

	public static final int MAX_DIFFERENCE = 10;

	private int target;
	private int diff;
	private Value[] values;
	private Set<Value> solutions; 
	
	private boolean abort = false;
	
	private Broadcaster broadcaster;

	public NumbersSolverThread(Handler handler, int[] numbers, int target) {
		this.broadcaster = new ThreadBroadcaster(handler);
		this.values = new Value[numbers.length];
		for(int i = 0; i < values.length; i++) {
			values[i] = new Value(numbers[i]);
		}

		this.target = target;
		this.solutions = new HashSet<Value>();
		this.diff = MAX_DIFFERENCE;
	} 
	
	public void abort() {
		abort = true;
	}

	@Override
	public void run() {
//		Thread.setDefaultUncaughtExceptionHandler(new DefaultExceptionHandler());
//		Log.i(Utils.TAG, "starting number solver thread...");
		solutions = new HashSet<Value>();
		diff = 10;
		Arrays.sort(values);
//		long t0 = System.currentTimeMillis();
		recursiveSolve(new Combination(values, 0));
//		long t1 = System.currentTimeMillis();
//		long t = t1 - t0;
		
		BroadcastEvent e = new BroadcastEvent(this).
				setEnum(ThreadEventType.COMPLETION_EVENT);
		broadcaster.fireEvent(e);
	}

	public List<String> getSolutions() {
		List<Value> values = new ArrayList<Value>(solutions);
		Comparator<Value> comp = new Comparator<Value>() {
			@Override
			public int compare(Value v1, Value v2) {
				if (v1.getProximity(target) < v2.getProximity(target))
					return -1;
				if (v1.getProximity(target) > v2.getProximity(target)) 
					return 1;
				if (v1.depth < v2.depth)
					return -1;
				if (v1.depth > v2.depth)
					return 1;
				return 0;
			}
		};
		Collections.sort(values, comp);

		List<String> retval = new ArrayList<String>(solutions.size());
		for(Value value : values) {
			retval.add(value.getEquation());
		}
		return retval;
	}

	//returns true if v is a better solution
	private void assess(Value v) {
		//		System.out.println(v.getEquation());
		int proximity = v.getProximity(target);
		if (proximity <= diff) {
			solutions.add(v);
//			Log.d(Utils.TAG, "SOLUTION: '" + v.getEquation() + "'");
			
			String status = solutions.size() + " solutions found, best \u00B1 " + diff;
			BroadcastEvent e = new BroadcastEvent(this).
					setEnum(ThreadEventType.STATUS_EVENT).
					setString(status);
			broadcaster.fireEvent(e);
			this.diff = proximity;
		}
	}

	private static class Combination {
		public int index;
		public Value[] values;
		public Combination(Value[] values, int index) {
			this.values = values;
			this.index = index;
		}
	}

	/*
	 * removes elements at indices x and y from a,
	 * and inserts v to maintain sorted order of a
	 * precondition: a is in sorted order to begin with
	 */
	private static Combination combine(Value[] a, Value v, int x, int y, Combination result) {

		Value[] retval = new Value[a.length - 1];
		int r = 0, i = 0, tmp = -1;

		while(i < a.length && a[i].val > v.val) {
			if (i != x && i != y)
				retval[r++] = a[i];
			i++;
		}
		tmp = r;
		retval[r++] = v;
		while(i < a.length) {
			if (i != x && i != y)
				retval[r++] = a[i];
			i++;
		}
		result.index = tmp;
		result.values = retval;

		return result;
	}

	private void recursiveSolve(Combination combination) {
		
		if (abort)
			return;

		Value[] numbers = combination.values;
		int index = combination.index;
		Value v;

		for(int i = index; i < numbers.length - 1; i++) {
			for(int j = i + 1; j < numbers.length; j++) {

				Value a = numbers[i];
				Value b = numbers[j];

				//addition
				v = new Value(a, b, '+', a.val + b.val);
				assess(v);
				if (numbers.length > 2) {
					Combination c = combine(numbers, v, i, j, combination);
					recursiveSolve(c);
				}

				//subtraction
				if (a.val > b.val) {
					v = new Value(a, b, '-', a.val - b.val);
					assess(v);
					if (numbers.length > 2) {
						Combination c = combine(numbers, v, i, j, combination);
						recursiveSolve(c);
					}
				}

				//multiplication
				if (b.val > 1) {
					v = new Value(a, b, MULT_CHAR, a.val * b.val);
					assess(v);
					if (numbers.length > 2) {
						Combination c = combine(numbers, v, i, j, combination);
						recursiveSolve(c);
					}
				}

				//division
				if (b.val > 1 && a.val % b.val == 0) {
					v = new Value(a, b, DIV_CHAR, a.val / b.val);
					assess(v);
					if (numbers.length > 2) {
						Combination c = combine(numbers, v, i, j, combination);
						recursiveSolve(c);
					}
				}
			}
		}
	}

	@Override
	public Broadcaster getBroadcaster() {
		return broadcaster;
	}

}
