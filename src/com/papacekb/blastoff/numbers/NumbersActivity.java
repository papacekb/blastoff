package com.papacekb.blastoff.numbers;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.papacekb.blastoff.Constants;
import com.papacekb.blastoff.Constants.ThreadEventType;
import com.papacekb.blastoff.CountdownThread;
import com.papacekb.blastoff.R;
import com.papacekb.blastoff.utils.Utils;
import com.papacekb.blastoff.utils.broadcast.BroadcastEvent;
import com.papacekb.blastoff.utils.broadcast.BroadcastListener;

public class NumbersActivity extends Activity implements BroadcastListener, View.OnClickListener {
	
	private NumbersSolverThread solverThread;
	private CountdownThread countdownThread;

	private int target;
	private int[] numbers = new int[6];
	private String[] solutions;
	private int time;

	private NumbersBag numbersBag = new NumbersBag();
	
	private boolean solving, counting;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_numbers);
		
		if (savedInstanceState != null) {
			numbers = savedInstanceState.getIntArray("numbers");
			target = savedInstanceState.getInt("target");
			solutions = savedInstanceState.getStringArray("solutions");
			solving = savedInstanceState.getBoolean("solving");
			counting = savedInstanceState.getBoolean("counting");
			time = savedInstanceState.getInt("time");
			
			if (solving) {
				//need to restart the solver thread
				startSolverThread();
			}
			if (counting) {
				startCountdownThread(time);
				setStatusVisible(true);
				setButtonsEnabled(false);
				setEndListenerActive(true);
			} else {
				updateSolutions(); 
			}
			updateTarget();
			updateNumbers();
			updateStatus();
		} else {
			Arrays.fill(numbers, 0);
			target = 0;
			solutions = new String[0];
			solving = false;
			counting = false;
			time = 0;
		}
	}
	
	@Override
	public void onStop() {
		super.onStop();
		endCountdownThread();
		endSolverThread();
	}
	
	@Override
	public void onSaveInstanceState(Bundle bundle) {
		super.onSaveInstanceState(bundle);
		bundle.putBoolean("solving", solving);
		bundle.putBoolean("counting", counting);
		bundle.putIntArray("numbers", numbers);
		bundle.putInt("target", target);
		bundle.putStringArray("solutions", solutions);
		bundle.putInt("time", time);
	}
	
	public void selectNumbers(View view) {
		switch(view.getId()) {
		case R.id.button0to6: begin(0); break;
		case R.id.button1to5: begin(1); break;
		case R.id.button2to4: begin(2); break;
		case R.id.button3to3: begin(3); break;
		case R.id.button4to2: begin(4); break;
		default: Log.e(Utils.TAG, "unknown view");
		}
	}

	//only called from View
//	public void end(View view) {
//		endCountdownThread();
//	}
	
	@Override
	public void onClick(View v) {
		endCountdownThread();
	}

	private void setEndListenerActive(boolean b) {
		View wrapper = findViewById(R.id.wrapper);
		if (b)
			wrapper.setOnClickListener(this);
		else
			wrapper.setOnClickListener(null);
	}
	
	private void setButtonsEnabled(boolean b) {
		((Button)findViewById(R.id.button0to6)).setEnabled(b);
		((Button)findViewById(R.id.button1to5)).setEnabled(b);
		((Button)findViewById(R.id.button2to4)).setEnabled(b);
		((Button)findViewById(R.id.button3to3)).setEnabled(b);
		((Button)findViewById(R.id.button4to2)).setEnabled(b);
	}
	
	private void setStatusVisible(boolean b) {
		View status = findViewById(R.id.status);
		status.setVisibility(b ? View.VISIBLE : View.GONE);
	}

	private void reset() {
		numbersBag.reset();
		Arrays.fill(numbers, 0);
		solutions = new String[]{};
		time = 0;
		updateNumbers();
		updateStatus();
		updateSolutions();
	}
	
	private void begin(int large) { 
		reset();	//reset every time just in case

		numbersBag.reset();

		int index = 0;

		//add the large numbers
		while(index < large) {
			int number = numbersBag.getLarge();
			numbers[index++] = number;
		} 

		//add the small numbers
		while(index < numbers.length) { 
			int number = numbersBag.getSmall();
			numbers[index++] = number;
		}

		target = calculateTarget(numbers);
		
		updateNumbers();
		
		updateTarget();

		setButtonsEnabled(false);
		setStatusVisible(true);
		setEndListenerActive(true);
		startSolverThread();
		startCountdownThread(Constants.TIME);
	}
	
	private static int calculateTarget(int [] nums) {

		Set<Integer> possibles = new HashSet<Integer>();

		for(int i = 101; i < 1000; i++)
			possibles.add(i);

		for (int i = 0; i < nums.length-1; i++) {
			for (int j = i+1; j < nums.length; j++) {
				possibles.remove(Integer.valueOf(nums[i] * nums[j]));
				possibles.remove(Integer.valueOf(nums[i] + nums[j]));
				possibles.remove(Integer.valueOf(Math.abs(nums[i] - nums[j])));
				if (nums[i] % nums[j] == 0)
					possibles.remove(nums[i] / nums[j]);
				if (nums[j] % nums[i] == 0)
					possibles.remove(nums[j] / nums[i]);
			}
		}
		//pick a random element from the set
		int r = Utils.rand.nextInt(possibles.size());
		int i = 0;
		for(Integer element : possibles) {
			if (i++ == r)
				return element;
		}
		//should have returned something by now!!
		throw new ArrayIndexOutOfBoundsException();
	}

	private void updateNumbers() {
		ViewGroup tileLayout = (ViewGroup)findViewById(R.id.tileLayout);
		for(int i = 0; i < 6; i++) {
			TextView tile = (TextView)tileLayout.getChildAt(i);
			String s = numbers[i] > 0 ? "" + numbers[i] : "";
			tile.setText(s);
		}
	}

	private void updateTarget() {
		TextView targetView = (TextView)findViewById(R.id.target);
		targetView.setText(target > 0 ? target + "" : "");
	}

	private void updateStatus() {
		TextView status = (TextView)findViewById(R.id.status);
		if (time <= 0)
			status.setText("");
		else
			status.setText(time + "");
	}

	private void updateSolutions() {
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.list_words, solutions);
		ListView list = (ListView)findViewById(R.id.solutions);
		list.setAdapter(adapter);
	}

	private void startSolverThread() {
		solverThread = new NumbersSolverThread(new Handler(), numbers, target);
		solverThread.getBroadcaster().addBroadcastListener(this);
		solving = true;
		solverThread.start();
	}

	private void startCountdownThread(int time) {
		countdownThread = new CountdownThread(new Handler(), time);
		countdownThread.getBroadcaster().addBroadcastListener(this);
		counting = true;
		countdownThread.start();
	}

	private void endSolverThread() {
		if (solving) {
			solverThread.abort();
			Log.i(Utils.TAG, Thread.currentThread().getName());
			try {
				solverThread.join();
			} catch(InterruptedException e) {
				Log.e(Utils.TAG, Log.getStackTraceString(e));
			}
		}
	}
	
	private void endCountdownThread() {
		if (counting) {
			countdownThread.abort = true;
			countdownThread.interrupt();
			try {
				countdownThread.join();
			} catch(InterruptedException e) {
				Log.e(Utils.TAG, Log.getStackTraceString(e));
			}
		}
	}

	private void handleCountdownEvent(BroadcastEvent e) {
		if (e.getEnum() == ThreadEventType.STATUS_EVENT) {
			time = e.getInt();
			updateStatus();
		} else if (e.getEnum() == ThreadEventType.COMPLETION_EVENT) {
			time = 0;
			updateStatus();
			
			counting = false;
			if (solving) {
				endSolverThread();
			} else {
				updateSolutions();
				setButtonsEnabled(true);
				setStatusVisible(false);
			}
		}
	}
	
	private void handleSolverEvent(BroadcastEvent e) {
			if (e.getEnum() == ThreadEventType.COMPLETION_EVENT) {
			solutions = solverThread.getSolutions().toArray(new String[0]);
			if (solutions.length == 0) {
				solutions = new String[] {Constants.NO_SOLUTIONS_MESSAGE};
			}
			solving = false;
			if (!counting) {
				updateSolutions();
				setEndListenerActive(false);
				setButtonsEnabled(true);
				setStatusVisible(false);
			}
		}
	}
	
	@Override
	public void handleBroadcastEvent(BroadcastEvent e) {
		if (e.getSource() == solverThread) {
			handleSolverEvent(e);
		} else if (e.getSource() == countdownThread) {
			handleCountdownEvent(e);
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.numbers, menu);
		return true;
	}
}
