package com.papacekb.blastoff.numbers;

import java.util.Collections;
import java.util.LinkedList;
import java.util.Queue;

public class NumbersBag {
	
	public static final int[] LARGE_POOL = new int[] {
		10, 25, 50, 100
	};
	
	public static final int[] SMALL_POOL = new int[] {
		1,1,2,2,3,3,4,4,5,5,6,6,7,7,8,8,9,9
	};
	
	private LinkedList<Integer> large;
	private LinkedList<Integer> small;
	
	private int getTile(Queue<Integer> source) {
		Integer s = source.poll();
		if (s != null) {
			return s;
		} else {
			//just in case
			reset();
			return source.poll();
		}
	}
	
	public int getLarge() {
		return getTile(large);
	}
	
	public int getSmall() {
		return getTile(small);
	}
	
	public NumbersBag() {
		large = new LinkedList<Integer>();
		small = new LinkedList<Integer>();
		reset();
	}
	
	public void reset() {
		large.clear();
		small.clear();
		for(int i : LARGE_POOL) {
			large.add(i);
		}
		for(int i : SMALL_POOL) {
			small.add(i);
		}
		Collections.shuffle(large);
		Collections.shuffle(small);
	}

}
