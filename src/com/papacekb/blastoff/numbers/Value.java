package com.papacekb.blastoff.numbers;

public class Value implements Comparable<Value> {
	
	public int val;
	public String path;
	public int depth; //number of terms
	
	public Value(int val) {
		this(val, 1, "" + val);
	}
	
	public Value(Value a, Value b, char op, int result) {
		this.val = result;
		this.depth = a.depth + b.depth;
		this.path = "( " + a.path + " " + op + " " + b.path + " )";
	}
	
	public Value(int val, int depth, String path) {
		this.val = val;
		this.depth = depth;
		this.path = path;
	}
	
	public int getProximity(int target) {
		return Math.abs(val - target);
	}

	/* sort in descending order by value */
	@Override
	public int compareTo(Value v) {
		if (this.val < v.val)
			return 1;
		if (this.val > v.val)
			return -1;
		return 0;
	} 
	
	@Override
	public boolean equals(Object o) {
		return (this.hashCode() == o.hashCode());
	}
	
	@Override
	public int hashCode() {
		return this.path.hashCode();
	}
	
	public String toString() { 
		return "" + this.val;
	}
	
	public String getEquation() {
		return this.path + " = " + this.val;
	}
}
