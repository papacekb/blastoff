package com.papacekb.blastoff.numbers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.papacekb.blastoff.Constants;
import com.papacekb.blastoff.Constants.ThreadEventType;
import com.papacekb.blastoff.R;
import com.papacekb.blastoff.utils.Utils;
import com.papacekb.blastoff.utils.broadcast.BroadcastEvent;
import com.papacekb.blastoff.utils.broadcast.BroadcastListener;

public class NumbersSolverActivity extends Activity implements BroadcastListener, View.OnClickListener {

	private int target;
	private int [] numbers;
	private String [] solutions;
	private boolean solving;
	private String status;
	
	private NumbersSolverThread solverThread;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_numbers_solver);
		initInputFilters();
		if (savedInstanceState != null) {
			target = savedInstanceState.getInt("target");
			numbers = savedInstanceState.getIntArray("numbers");
			solutions = savedInstanceState.getStringArray("solutions");
			status = savedInstanceState.getString("status");
			solving = savedInstanceState.getBoolean("solving");
			
			if (solving) {
				//need to restart the solver thread
				setProgressBarVisible(true);
				setEndListenerActive(true);
				startSolverThread();
			}
			updateTarget();
			updateNumbers();
			updateSolutions();
			updateStatus();
		} else {
			target = 0;
			numbers = new int[6];
			solutions = new String[]{};
			status = "";
			solving = false;
		}
	}
	
	@Override
	protected void onSaveInstanceState(Bundle bundle) {
		super.onSaveInstanceState(bundle);
		bundle.putIntArray("numbers", numbers);
		bundle.putBoolean("solving", solving);
		bundle.putStringArray("solutions", solutions);
		bundle.putString("status", status);
		bundle.putInt("target", target);
	}
	
	public void clear(View view) {
		target = 0;
		Arrays.fill(numbers, 0);
		status = "";
		solutions = new String[]{};
		
		updateTarget();
		updateNumbers();
		updateStatus();
		updateSolutions();
	}

	public void solve(View view) {
		EditText targetField = (EditText)findViewById(R.id.target);
		String targetStr = targetField.getText().toString().trim();
		
		if (!targetStr.matches("\\d+")) 
			return;
		 
		target = Integer.parseInt(targetStr);
	
		List<Integer> list = new ArrayList<Integer>();
		ViewGroup tileLayout = (ViewGroup)findViewById(R.id.tileLayout);
		for(int i = 0; i < tileLayout.getChildCount(); i++) {
			EditText tileField = (EditText)tileLayout.getChildAt(i);
			String tile = tileField.getText().toString().trim();
			if (tile.matches("\\d+")) {
				list.add(Integer.parseInt(tile));
			}
		}
		
		if (list.isEmpty())
			return;
		
		for(int i = 0; i < list.size(); i++)
			numbers[i] = list.get(i);
		
		status = "0 solutions found";
		solutions = new String[]{};
		updateStatus();
		updateSolutions();
		setEndListenerActive(true);
		setProgressBarVisible(true);
		startSolverThread();
		
		hideSoftInput();
	}

//	public void end(View view) {
//		endSolverThread();
//	}
	
	@Override
	public void onClick(View v) {
		endSolverThread();
	}

	private void setEndListenerActive(boolean b) {
		View wrapper = findViewById(R.id.wrapper);
		if (b)
			wrapper.setOnClickListener(this);
		else
			wrapper.setOnClickListener(null);
	}
	
	private void setProgressBarVisible(boolean b) {
		//show the progress bar
		ProgressBar progressBar = (ProgressBar)findViewById(R.id.progressBar1);
		progressBar.setVisibility(b ? View.VISIBLE : View.GONE);
	}

	private void hideSoftInput() {
		View view = getCurrentFocus();
		InputMethodManager imm = (InputMethodManager)getSystemService(
				Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
	}

	private void initInputFilters() {
		InputFilter filter = new InputFilter() {
			@Override
			public CharSequence filter(CharSequence src, int start, int end,
					Spanned dest, int dstart, int dend) {
				for(int i = start; i < end; i++) {
					if (!Character.isDigit(src.charAt(i)))
						return "";
				}
				return null;
			}
		};
		InputFilter[] filters = new InputFilter[] {
				filter, 
				new InputFilter.AllCaps(),
				new InputFilter.LengthFilter(Constants.MAX_DIGITS)
		};
		EditText target = (EditText)findViewById(R.id.target);
		ViewGroup tileLayout = (ViewGroup)findViewById(R.id.tileLayout);
		target.setFilters(filters);
		for(int i = 0; i < tileLayout.getChildCount(); i++) {
			EditText tile = (EditText)tileLayout.getChildAt(i);
			tile.setFilters(filters);
		}
	}

	private void startSolverThread() {
		solverThread = new NumbersSolverThread(new Handler(), numbers, target);
		solverThread.getBroadcaster().addBroadcastListener(this);
		solving = true;
		solverThread.start();
	}

	private void endSolverThread() {
		if (solving) {
			solverThread.abort();
			try {
				solverThread.join();
			} catch(InterruptedException e) {
				Log.e(Utils.TAG, Log.getStackTraceString(e));
			}
		}
	}

	private void updateNumbers() {
		ViewGroup tileLayout = (ViewGroup)findViewById(R.id.tileLayout);
		for(int i = 0; i < 6; i++) {
			EditText tile = (EditText)tileLayout.getChildAt(i);
			String s = numbers[i] > 0 ? "" + numbers[i] : "";
			tile.setText(s);
		}
	}
	
	private void updateTarget() {
		TextView targetView = (TextView)findViewById(R.id.target);
		targetView.setText(target > 0 ? target + "" : "");
	}
	
	private void updateStatus() {
		TextView statusView = (TextView)findViewById(R.id.status);
		statusView.setText(status);
	}

	private void updateSolutions() {
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.list_words, solutions);
		ListView list = (ListView)findViewById(R.id.solutions);
		list.setAdapter(adapter);
	}

	@Override
	public void handleBroadcastEvent(BroadcastEvent e) {
		if (e.getEnum() == ThreadEventType.STATUS_EVENT) { 
			status = e.getString();
			updateStatus();
		}
		if (e.getEnum() == ThreadEventType.COMPLETION_EVENT) { 
			solutions = solverThread.getSolutions().toArray(new String[0]);
			solving = false;
			setEndListenerActive(false);
			updateSolutions();
			setProgressBarVisible(false);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.numbers_solver, menu);
		return true;
	}

}
