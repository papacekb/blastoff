package com.papacekb.blastoff;

public class Constants {
	
	//the most solutions that should be found
	public static final int MAX_SOLUTIONS = 1000;
	
	//the number of letters tiles
	public static final int NUM_LETTERS = 9;
	
	public static final int NUM_NUMBERS = 6;
	
	//the most digits that may appear on any tile in a numbers game
	public static final int MAX_DIGITS = 3;
	
	//seconds allowed in puzzle games
	public static final int TIME = 30;
	
	public static final String NO_SOLUTIONS_MESSAGE = "-- no solutions found --";
	
	public enum ThreadEventType {
		STATUS_EVENT, COMPLETION_EVENT
	}
	

}
