package com.papacekb.blastoff.letters;

import java.util.Locale;

import android.app.Activity;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.papacekb.blastoff.Constants;
import com.papacekb.blastoff.Constants.ThreadEventType;
import com.papacekb.blastoff.CountdownThread;
import com.papacekb.blastoff.LexiconDb;
import com.papacekb.blastoff.R;
import com.papacekb.blastoff.utils.Utils;
import com.papacekb.blastoff.utils.broadcast.BroadcastEvent;
import com.papacekb.blastoff.utils.broadcast.BroadcastListener;

public class LettersActivity extends Activity implements BroadcastListener, View.OnClickListener {

	private LettersBag letterBag = new LettersBag();

	private String letters = "";
	private String [] solutions;

	private SQLiteDatabase db;

	private CountdownThread countdownThread;
	private LettersSolverThread solverThread;

	private boolean solving;
	private boolean counting;

	private int time;

	@Override
	protected void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_letters);

		db = new LexiconDb(this).getReadableDatabase();
		if (savedInstanceState != null) {
			letters = savedInstanceState.getString("letters");
			solutions = savedInstanceState.getStringArray("solutions");
			solving = savedInstanceState.getBoolean("solving");
			counting = savedInstanceState.getBoolean("counting");
			time = savedInstanceState.getInt("time");

			if (solving) {
				//need to restart the solver thread
				startSolverThread();
			}
			if (counting) {
				startCountdownThread(time);
				setButtonsEnabled(false);
				setEndListenerActive(true);
				setStatusVisible(true);
			} else {
				updateSolutions();
			}
			updateLetters();
			updateStatus();
		} else {
			letters = "";
			solutions = new String[0];
			solving = false;
			counting = false;
			time = 0;
		}
	}

	@Override
	public void onStop() {
		super.onStop();
		endCountdownThread();
		endSolverThread();
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		db.close();
	}

	@Override
	public void onSaveInstanceState(Bundle bundle) {
		super.onSaveInstanceState(bundle);
		bundle.putBoolean("solving", solving);
		bundle.putBoolean("counting", counting);
		bundle.putString("letters", letters);
		bundle.putStringArray("solutions", solutions);
		bundle.putInt("time", time);
	} 

	public void selectVowel(View view) {
		char c = letterBag.getVowel();
		addLetter(c + "");
	}

	public void selectConsonant(View view) {
		char c = letterBag.getConsonant();
		addLetter(c + "");
	}
	
	@Override
	public void onClick(View v) {
		endCountdownThread();
	}

	private void setEndListenerActive(boolean b) {
		View wrapper = findViewById(R.id.wrapper);
		if (b)
			wrapper.setOnClickListener(this);
		else
			wrapper.setOnClickListener(null);
	}
	
	

//	//only called from view
//	public void end(View view) {
//		endCountdownThread();
//	}

	private void setButtonsEnabled(boolean b) {
		Button vowelButton = (Button)findViewById(R.id.vowelButton);
		vowelButton.setEnabled(b);
	
		Button consonantButton = (Button)findViewById(R.id.consonantButton);
		consonantButton.setEnabled(b);
	}

	private void setStatusVisible(boolean b) {
		View status = findViewById(R.id.status);
		status.setVisibility(b ? View.VISIBLE : View.GONE);
	}

	private void reset() {
		letterBag.reset();
		letters = "";
		solutions = new String[0];
		updateLetters();
		updateSolutions();
	}

	private void addLetter(String letter) {
		if (counting) {
			return;
		}
		if (letters.length() >= Constants.NUM_LETTERS) {
			reset();
		}
		if (letters.length() < Constants.NUM_LETTERS) {
			letters += letter.toLowerCase(Locale.getDefault());
			if (letters.length() == Constants.NUM_LETTERS) {
				begin();
			}
		}
		updateLetters();
	}

	private void begin() {
		setButtonsEnabled(false);
		setEndListenerActive(true);
		setStatusVisible(true);
		startSolverThread();
		startCountdownThread(Constants.TIME);
	}
	
	private void updateLetters() {
		ViewGroup tileLayout = (ViewGroup)findViewById(R.id.tileLayout);
		for(int i = 0; i < Constants.NUM_LETTERS; i++) {
			TextView tile = (TextView)tileLayout.getChildAt(i);
			String s = i < letters.length() ? "" + Character.toUpperCase(letters.charAt(i)) : "";
			tile.setText(s);
		}
	}

	private void updateSolutions() {
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.list_words, solutions);
		ListView list = (ListView)findViewById(R.id.solutions);
		list.setAdapter(adapter);
	}

	private void updateStatus() {
		TextView status = (TextView)findViewById(R.id.status);
		if (time <= 0)
			status.setText("");
		else
			status.setText(time + "");
	}

	private void startSolverThread() {
		solverThread = new LettersSolverThread(new Handler(), db, letters);
		solverThread.getBroadcaster().addBroadcastListener(this);
		solving = true;
		solverThread.start();
	}

	private void startCountdownThread(int time) {
		countdownThread = new CountdownThread(new Handler(), time);
		countdownThread.getBroadcaster().addBroadcastListener(this);
		counting = true;
		countdownThread.start();
	}

	private void endSolverThread() {
		if (solving) {
			solverThread.abort();
			try {
				solverThread.join();
			} catch(InterruptedException e) {
				Log.e(Utils.TAG, Log.getStackTraceString(e));
			}
		}
	}

	private void endCountdownThread() {
		if (counting) {
			countdownThread.abort = true;
			countdownThread.interrupt();
			try {
				countdownThread.join();
			} catch(InterruptedException e) {}
		}
	}

	private void handleSolverEvent(BroadcastEvent e) {
		if (e.getEnum() == ThreadEventType.COMPLETION_EVENT) {
			solutions = solverThread.getSolutions().toArray(new String[0]);
			if (solutions.length == 0) {
				solutions = new String[] {Constants.NO_SOLUTIONS_MESSAGE};
			}
			solving = false;
			if (!counting) {
				updateSolutions();
				setButtonsEnabled(true);
				setStatusVisible(false);
			}
		}
	}

	private void handleCountdownEvent(BroadcastEvent e) {
		if (e.getEnum() == ThreadEventType.STATUS_EVENT) {
			time = e.getInt();
			updateStatus();
		} else if (e.getEnum() == ThreadEventType.COMPLETION_EVENT) {
			time = 0;

			counting = false;
			if (solving) {
				endSolverThread();
			} else {
				updateSolutions();
				setButtonsEnabled(true);
				setEndListenerActive(false);
				setStatusVisible(false);
			}
		}
	}

	@Override
	public void handleBroadcastEvent(BroadcastEvent e) {
		if (e.getSource() == solverThread) {
			handleSolverEvent(e);
		} else if (e.getSource() == countdownThread) {
			handleCountdownEvent(e);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.letters, menu);
		return true;
	}

}
