package com.papacekb.blastoff.letters;

import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.text.InputFilter;
import android.text.Spanned;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.papacekb.blastoff.Constants;
import com.papacekb.blastoff.Constants.ThreadEventType;
import com.papacekb.blastoff.LexiconDb;
import com.papacekb.blastoff.R;
import com.papacekb.blastoff.utils.broadcast.BroadcastEvent;
import com.papacekb.blastoff.utils.broadcast.BroadcastListener;

public class LettersSolverActivity extends Activity implements BroadcastListener, View.OnClickListener {
	
	private SQLiteDatabase db;
	
	private LettersSolverThread solverThread;
	private String letters;
	private String[] solutions;
	private String status;
	private boolean solving;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_letters_solver);
		initInputFilters();
		db = new LexiconDb(this).getReadableDatabase();
		
		if (savedInstanceState != null) {
			letters = savedInstanceState.getString("letters");
			solutions = savedInstanceState.getStringArray("solutions");
			solving = savedInstanceState.getBoolean("solving");
			status = savedInstanceState.getString("status");
			if (solving) {
				//need to restart the solver thread
				setEndListenerActive(true);
				setProgressBarVisible(true);
				startSolverThread();
			}
			updateSolutions();
			EditText text = (EditText)findViewById(R.id.lettersText); 
			text.setText(letters);
			updateStatus();
		} else {
			letters = "";
			solutions = new String [] {};
			solving = false;
			status = "";
		}
	}
	
	@Override
	public void onStop() {
		super.onStop();
		endSolverThread();
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		db.close();
	}
	
	@Override
	public void onSaveInstanceState(Bundle bundle) {
		super.onSaveInstanceState(bundle);
		bundle.putString("letters", letters);
		bundle.putBoolean("solving", solving);
		bundle.putStringArray("solutions", solutions);
		bundle.putString("status", status);
	}
	
	public void clear(View view) {
		EditText textField = (EditText)findViewById(R.id.lettersText);
		textField.setText("");
		
		solutions = new String[]{};
		updateSolutions();
		
		TextView statusView = (TextView)findViewById(R.id.status);
		statusView.setText("");
	}

	public void solve(View view) {
		EditText text = (EditText)findViewById(R.id.lettersText);
		letters = text.getText().toString();
		letters = letters.toLowerCase(Locale.getDefault());
		
		status = "0 solutions found";
		solutions = new String [] {};
		updateStatus();
		updateSolutions();
		
		startSolverThread();
		setEndListenerActive(true);
		setProgressBarVisible(true);
		hideSoftInput();
	}
	
	@Override
	public void onClick(View v) {
		endSolverThread();
	}
	
	private void setEndListenerActive(boolean b) {
		View wrapper = findViewById(R.id.wrapper);
		if (b)
			wrapper.setOnClickListener(this);
		else
			wrapper.setOnClickListener(null);
	}

	private void initInputFilters() { 
		EditText text = (EditText)findViewById(R.id.lettersText);
		InputFilter filter = new InputFilter() {
			@Override
			public CharSequence filter(CharSequence src, int start, int end,
					Spanned dest, int dstart, int dend) {
				for(int i = start; i < end; i++) {
					if (!Character.isLetter(src.charAt(i)))
						return "";
				}
				return null;
			}
		};
		text.setFilters(new InputFilter[] {
				filter, 
				new InputFilter.AllCaps(), 
				new InputFilter.LengthFilter(Constants.NUM_LETTERS)
		});
	}

	private void setProgressBarVisible(boolean b) {
		ProgressBar progressBar = (ProgressBar)findViewById(R.id.progressBar1);
		progressBar.setVisibility(b ? View.VISIBLE : View.GONE);
	}

	private void hideSoftInput() {
		View view = getCurrentFocus();
		InputMethodManager imm = (InputMethodManager)getSystemService(
				Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
	}
	
	private void startSolverThread() {
		solverThread = new LettersSolverThread(new Handler(), db, letters);
		solverThread.getBroadcaster().addBroadcastListener(this);
		solving = true;
		solverThread.start();
	}
	
	private void endSolverThread() {
		if (solving) {
			solverThread.abort();
			try {
				solverThread.join();
			} catch(InterruptedException e) {}
		}
	}
	
	private void updateSolutions() {
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.list_words, solutions);
		ListView list = (ListView)findViewById(R.id.solutions);
		list.setAdapter(adapter);
	}
	
	private void updateStatus() {
		TextView statusView = (TextView)findViewById(R.id.status);
		statusView.setText(status);
	}
	
	@Override
	public void handleBroadcastEvent(BroadcastEvent e) {
		if (e.getEnum() == ThreadEventType.STATUS_EVENT) {
			status = e.getString();
			updateStatus();
		} else if (e.getEnum() == ThreadEventType.COMPLETION_EVENT) {
			solutions = solverThread.getSolutions().toArray(new String[0]);
			solving = false;
			setEndListenerActive(false);
			updateSolutions();
			setProgressBarVisible(false);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.letters_solver, menu);
		return true;
	}

}
