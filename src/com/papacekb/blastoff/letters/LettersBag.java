package com.papacekb.blastoff.letters;

import java.util.Collections;
import java.util.LinkedList;
import java.util.Queue;

public class LettersBag {

	public static final String CONSONANT_POOL = 
					"BB" +				//2 x B
					"CC" +				//2 x C
					"DDDD" +			//4 x D
					"FF" +				//2 x F
					"GGG" +				//3 x G
					"HH" +				//2 x H
					"J" +				//1 x J
					"K" +				//1 x K
					"LLLL" +			//4 x L
					"MM" +				//2 x M
					"NNNNNN" +			//6 x N
					"PP" +				//2 x P
					"Q" +				//1 x Q
					"RRRRRR" +			//6 x R
					"SSSS" +			//4 x S
					"TTTTTT" +			//6 x T
					"VV" +				//2 x V
					"WW" +				//2 x W
					"X" +				//1 x X
					"YY" +				//2 x Y
					"Z";				//1 x Z

	public static final String VOWEL_POOL =
					"AAAA" +		//9 x A 4
					"EEEEEE" +		//12 x E 6
					"IIII" +		//9 x I 4
					"OOOO" +		//8 x O 4
					"UU";			//4 x U 2

	private LinkedList<Character> vowels;
	private LinkedList<Character> consonants;
	
	public LettersBag() {
		vowels = new LinkedList<Character>();
		consonants = new LinkedList<Character>();
		reset();
	}
	
	public void reset() {
		vowels.clear();
		consonants.clear();
		for(Character c : VOWEL_POOL.toCharArray()) {
			vowels.add(c);
		}
		for(Character c : CONSONANT_POOL.toCharArray()) {
			consonants.add(c);
		}
		Collections.shuffle(vowels);
		Collections.shuffle(consonants);
	}
	
	public char getVowel() {
		return getLetter(vowels);
	}
	
	public char getConsonant() {
		return getLetter(consonants);
	}
	
	private char getLetter(Queue<Character> source) {
		Character v = source.poll();
		if (v != null) {
			return v;
		} else {
			//just in case
			reset();
			return source.poll(); 
		}
	}
	
	public static void main(String [] args) {
		LettersBag lb = new LettersBag();
		for(int i = 0; i < 10; i++) {
			System.out.println(lb.getConsonant());
			System.out.println(lb.getVowel());
		}
	}

}
