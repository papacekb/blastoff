package com.papacekb.blastoff.letters;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;
import android.util.Log;

import com.papacekb.blastoff.Constants;
import com.papacekb.blastoff.Constants.ThreadEventType;
import com.papacekb.blastoff.utils.Utils;
import com.papacekb.blastoff.utils.broadcast.BroadcastEvent;
import com.papacekb.blastoff.utils.broadcast.Broadcaster;
import com.papacekb.blastoff.utils.broadcast.Broadcasting;
import com.papacekb.blastoff.utils.broadcast.ThreadBroadcaster;

public class LettersSolverThread extends Thread implements Broadcasting {

	private String letters;

	private List<String> solutions;
	private SQLiteDatabase db;
	private Broadcaster broadcaster;
	
	private boolean abort = false;

	//letters should be lower-case alpha only
	public LettersSolverThread(Handler handler, SQLiteDatabase db, String letters) {
		this.db = db;
		this.broadcaster = new ThreadBroadcaster(handler);
		this.letters = new String(letters);
		this.solutions = new ArrayList<String>(Constants.MAX_SOLUTIONS);
	}


	public List<String> getSolutions() {
		return this.solutions;
	}
	
	public void abort() {
		abort = true;
	}

	@Override
	public void run() {
		List<String> combinations = getCombinations(letters);

		try {
			Cursor cursor;
			int best = 0;
			for(String s : combinations) {
				String table = "lex" + s.length();
				String sql = "SELECT word FROM " + table + " WHERE letters = ?";
				cursor = db.rawQuery(sql, new String[]{s});

				while(cursor.moveToNext()) {
					String soln = cursor.getString(0);	
					if (best == 0)
						best = soln.length();
					solutions.add(soln + " (" + soln.length() + ")");

					String status = solutions.size() + " solutions found, best " + 
							best + " letters";
					broadcaster.fireEvent(new BroadcastEvent(this).
							setEnum(ThreadEventType.STATUS_EVENT).
							setString(status));

					if (abort || solutions.size() >= Constants.MAX_SOLUTIONS)
						break;
				}
				cursor.close();
				if (abort || solutions.size() >= Constants.MAX_SOLUTIONS)
					break;
			}
		} catch(Exception e) {
			Log.e(Utils.TAG, Log.getStackTraceString(e));
		}
		broadcaster.fireEvent(new BroadcastEvent(this).
				setEnum(ThreadEventType.COMPLETION_EVENT));
	}

	//recursive
	public static void combine(Set<String> result, String prefix, String elements, int k) {
		if (k == 0) {
			result.add(prefix);
			return;
		}
		for (int i = 0; i < elements.length(); i++)
			combine(result, prefix + elements.charAt(i), elements.substring(i + 1), k - 1);
	}

	private static List<String> getCombinations(String s) {
		s = sortString(s);
		Set<String> combs = new HashSet<String>(400);
		for(int i = s.length(); i >= 4; i--) {
			combine(combs, "", s, i);
		}

		List<String> result = new ArrayList<String>(combs);
		Comparator<String> comparator = new Comparator<String>() {
			@Override
			public int compare(String s0, String s1) {
				if (s0.length() > s1.length())
					return -1;
				if (s0.length() < s1.length())
					return 1;
				return s0.compareTo(s1);
			}
		};
		Collections.sort(result, comparator);
		return result;
	}
	
	public static String sortString(String s) {
		char[] content = s.toCharArray();
		Arrays.sort(content);
		return new String(content);
	}

	@Override
	public Broadcaster getBroadcaster() {
		return broadcaster;
	}

}
