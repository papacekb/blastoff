package com.papacekb.blastoff;

import android.content.Context;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

public class LexiconDb extends SQLiteAssetHelper {
	
	private static final String DATABASE_NAME = "lex";
	private static final int DATABASE_VERSION = 1;
	
	public LexiconDb(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

}
