package com.papacekb.blastoff;

import android.os.Handler;

import com.papacekb.blastoff.Constants.ThreadEventType;
import com.papacekb.blastoff.utils.broadcast.BroadcastEvent;
import com.papacekb.blastoff.utils.broadcast.Broadcaster;
import com.papacekb.blastoff.utils.broadcast.Broadcasting;
import com.papacekb.blastoff.utils.broadcast.ThreadBroadcaster;

public class CountdownThread extends Thread implements Broadcasting {
	
	public boolean abort;
	private Broadcaster broadcaster;
	
	//time in seconds
	private int time;
	
	public CountdownThread(Handler handler, int time) {
		this.broadcaster = new ThreadBroadcaster(handler);
		this.time = time;
	}
	
	@Override
	public void run() {
		while(!abort) {
			broadcaster.fireEvent(new BroadcastEvent(this).
					setEnum(ThreadEventType.STATUS_EVENT).
					setInt(time--));
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				break;
			}
			if (time <= 0)
				break;
		}
		broadcaster.fireEvent(new BroadcastEvent(this).
				setEnum(ThreadEventType.COMPLETION_EVENT));
	}

	@Override
	public Broadcaster getBroadcaster() {
		return broadcaster;
	}

}
